#!/usr/bin/python3
'''
                Sun 02 Aug 2020 07:04:59 AM

        this is brought to you by a team of headless chooks
    it is yours to do whatever you want.
    it comes with no claims to do anything, nothing.
    it might grab json data from tatts / ubet / qtab whatever,
    and it might give a sqlite database contining data
db_dir: change where you might want your data
db_db:   change to whatever you might want for your datatabase name
table:  change to what seems a good name for a database table.
.......
             most interesting json keys:
 "RaceDay": {
  "MeetingDate":
  "Meetings": [
   {
    "MeetingCode":
    "VenueName":
    "WeatherCondition":
    "TrackCondition":
    "TrackRating":
....
    "Races": [
      "Status":
 "SELLING",
      "Tips",
      "RaceNumber":
      "RaceTime": "2020-07-29T11:57:00",
      "RaceName": "TAB LONG MAY WE PLAY CG&E MAIDEN PLATE",
      "Distance": 1200,
...........
      "Runners": [
       {
        "RunnerNumber":
        "RunnerName":
        "RiderName":
        "RiderChanged":
        "Barrier":
        "Weight":
        "LastThreeStarts":
        "Form":
        "Rating":
        "Scratched":
        "LateScratching":
        "WinOdds":
        "PlaceOdds":
        "FPWinOdds":
        "FPPlaceOdds":
       },
.................................

https://api.tatts.com/svc/sales/vmax/web/data/racing/2020/07/29/BR1

 Necessity is mother of all evils
        20181219 shortly before 12 local time
           ubet shuttered tatts.com and took our data

 However the XML site is still alive.
  it contains all the race data. it even contains quinella odds

 in desperation I put this together

     2020  pull finger out
   in desperation i decided json is best

0/   inseert local directory at the beginning of the python exec path

1/ ubet SSL certificates are not in python
        so that is bypassed

'''
#
Local = "/usr/local/share/public/"
#
db_dir = "%s/db" % Local
db_db  = "qtab_json.db"
table = "qtab_json"
#
from datetime import date
import json
import os
from os.path import exists
#
#       regex is more stable for me, but re is standard
#
import re
#import regex as re

import sys
import sqlite3
#
#       postgreSQL : fix the usedb and you have a real SQL database
#
#import psycopg2

from urllib.request import Request,urlopen
#
# stack overflow 27835619
#
# turn off verification. (for tab.ubet.com)
import ssl
ssl._create_default_https_context = ssl._create_unverified_context
#
sys.path.insert( 0, os.curdir )
#
#------------------------------------------------------------------
#
def read_mee( url ):
  '''
         read TAB   nz or QTAB
      read  json site

           #tp://stackoverflow.com/questions/9839179/https-request-in-py
url = "https://www.tab.co.nz/racing/"
url = "https://www.tab.co.nz/odds/"
            #tp://stackoverflow.com/questions/9839179/https-request-in-py

  '''
#
#         U_A fakes a browser 
#                             TABs require a browser
#
  U_A = 'Mozilla/5.0(Windows:U;Windows NT 5.1;it;rv;1.8.1.11)Gecko/20071127 Firefox/2.0.0.91'
#
  REQ = Request( url=url, headers={ 'User-Agent': U_A }, method='GET' )
#
  with urlopen(REQ) as xex:
    bla = xex.read().decode()
  return json.loads( bla )
#
#------------------------------------------------------------------
#
def lib_sql_write( conn,curs, table, dictda ):
  '''

  '''
  sqins = "insert into %s( " % table
  sqvals = " values( "

  for dakey in dictda.keys():
    sqins = "%s%s, " %( sqins, dakey )
    sqvals = "%s\'%s\', " %( sqvals, dictda[ dakey ] )
  sqins = "%s)" %( sqins )
  sqins = sqins.replace( ', )', " )" )
  sqvals = "%s)" %( sqvals )
  sqvals = sqvals.replace( ', )', " ) on conflict do nothing" )
  SQEX = "%s%s" %(sqins, sqvals)
#  print('...',SQEX,'...')
  curs.execute( SQEX )
  conn.commit()
#
#------------------------------------------------------------------
#
def lib_usedb( sqdir, sqfi ):
  '''
         sqlite uses ordinary files
     1/ test file if it does not exist == error
     2/ sqlite blocks on write. pragma turns that off


  replace with postgreSQL ?

import psycopg2

con =  psycopg2.connect( database='new001',port='5757',host='localhost' )
cur = con.cursor()


  '''
  db = "%s/%s" %( sqdir, sqfi )
  try:
    fi = open( db,'r' )
    fi.close()
    conn = sqlite3.connect( db )
    curs = conn.cursor()
    curs.execute( "PRAGMA synchronous = off" )
    return curs,conn
  except:
    print( "\n%s\n * * * * * database does not exist * * * * * *\n" % ( db ) )
    exit( -4 )
#
#------------------------------------------------------------------
#
def lib_create_db( sqdir, sqfi ):
  '''
        does nothing if sqdir is not a directory
  '''
  if os.path.isdir( sqdir ):
    sqfil = sqfi.lower()
    sqdb = "%s/%s" %( sqdir, sqfil )
    if os.path.isfile( sqdb ):
      pass
    else:
      sqf = open( sqdb, "w" )
      sqf.close()
    return( lib_usedb( sqdir, sqfil ))
#
#------------------------------------------------------------------
#
def lib_sq_table( curs, table, columns, constraint ):
  '''
     returns sqlite code to create table
                 and a dictionary where keys are column names
                        values are None

race time is yyyy-mm-ddTnn:nn:nn

QTAB_ME_COLUMNS = { "qtab_pk": " integer constraint qtab_pk PRIMARY KEY autoincrement",         \
"MeetingDate":varchar, 
"MeetingCode":varchar, 
"VenueName":varchar, 
"WeatherCondition":varchar, 
"TrackCondition":varchar, 
"TrackRating":varchar, 
"RaceNumber": num,
"RaceTime":varchar,
"RaceName":varchar,
"Distance":num,
"RunnerNumber":num,
"RunnerName":varchar,
"RiderName":varchar,
"RiderChanged":varchar,
"Barrier":num,
"Weight":real,
"LastThreeStarts":varchar,
"Form":varchar,
"Rating":num,
"Scratched":varchar,
"LateScratching":varchar,
"WinOdds":real,
"PlaceOdds":real,
"FPWinOdds":real,
"FPPlaceOdds":real
}
  '''
  ta_cols = ""
  sqid = {}
  for doco in columns.keys():
    ta_cols = "%s%s %s, " %( ta_cols, doco, columns[ doco ] )
#
#      do not place primar y key in data dict
#
    if "autoincrement" in columns[ doco ].lower():     continue
    if "primary key"   in columns[ doco ].lower():     continue
    sqid[ doco ] = None
  if constraint != None:
    if len( constraint ) > 2:
      SQC = 'CREATE TABLE IF NOT EXISTS %s( %s UNIQUE( %s ))' %( table, ta_cols, constraint )
      return SQC, sqid
  SQC = 'CREATE TABLE IF NOT EXISTS %s( %s )' %( table, ta_cols )
  return SQC.replace( ',  )', " )" ), sqid
#
#------------------------------------------------------------------
#
'''       the defining bits           '''
#
num = integer = 'int'
varchar = char = 'varchar'
real = 'real'
#
# "RiderName":varchar,"RiderChanged":varchar,"Barrier":num,"Weight":real,       \
#
#      sql database with one table, normalise = 0
#             4 Aug 2020  add tips[0]
#
QTAB_ME_COLUMNS = { "qtab_pk":"integer constraint qtab_pk PRIMARY KEY autoincrement",         \
 "MeetingDate":varchar,"MeetingCode":varchar,"VenueName":varchar,"WeatherCondition":varchar,  \
 "TrackCondition":varchar,"TrackRating":varchar,"RaceNumber":num,"RaceTime":varchar,          \
 "Status":varchar, "RaceName":varchar,"Distance":num,"RunnerNumber":num,"RunnerName":varchar, \
 "RiderName":varchar,"RiderChanged":varchar,"Barrier":num,"Weight":real,"Tips":varchar,       \
 "LastThreeStarts":varchar,"Form":varchar,"Rating":num,"Scratched":varchar,                   \
 "LateScratching":varchar,"WinOdds":real,"PlaceOdds":real,"FPWinOdds":real,"FPPlaceOdds":real \
                 }
QTAB_ME_CONSTRAINT = "RunnerName,MeetingDate,VenueName,RaceNumber"
#
#------------------------------------------------------------------
#
if __name__ == "__main__":
#  UR = "https://api.tatts.com/svc/sales/vmax/web/data/racing/2020/07/29/BR1"
  UR = "https://api.tatts.com/svc/sales/vmax/web/data/racing/"
#
#     debug
#
#  db_dir = "/tmp"
#  db_db  = "blat.db"
#
#      sqlite database with one table, normalise = 0
#
  curs,con = lib_create_db( db_dir,db_db )
#
#      sqlite code, python dictionary
#
  sqex,sq_dic_1 = lib_sq_table( curs,table,QTAB_ME_COLUMNS,QTAB_ME_CONSTRAINT )
  curs.execute( sqex )
  con.commit()
#
#    today
#
  tdom = date.today().strftime( "%Y/%m/%d" )
#  print(tdom)
  UR = "%s/%s" %( UR,tdom )
#
#      Try to read our qcode json
#
  reaj = read_mee(UR)
  if len( reaj ) < 1:
    print(reaj)
    print(len(reaj))
    print("\n%sJSON read failed" %( sys.argv[0] ))
    exit( -1 )
#
#     debug
#
#  fo = open("/tmp/foall.txt",'w')
#  fo.write(json.dumps( reaj,indent=1))
#  fo.close()
#
#     meetings list from raceday
#
  meej = reaj['RaceDay']['Meetings']
  sq_dic_1['MeetingDate'] = tdom
#
  for aco in range(len(meej)):
    sq_dic_2 = sq_dic_1.copy()
    sq_dic_2['MeetingCode'] = qcode = meej[aco]['MeetingCode']
    if qcode[-1] == 'S':              continue
#    if qcode[-1] == 'G':              print("GGGGGGGGGGGGGGGGGG",":::%s:::" %qcode)
#    if qcode[-1] == 'T':              print("TTTTTTTTTTTTTTTTTT","::%s::" %qcode)
#
#       uncomment to ignore 'G' & 'T'
#
#    if qcode[-1] != 'R':              continue
    sq_dic_2['VenueName'] = meej[aco]['VenueName']
    URL = "%s/%s" %( UR,qcode )
    reaj = read_mee(URL)
#
#      debug
#
#    print(URL,'+++++++++++\n',json.dumps( reaj,indent=0),'============\n')

#
#      sqlite dislikes "'" in a data string. I substitute "^"
#          i could escape ' but ....
#
    nora = len( reaj['RaceDay']['Meetings'][0]['Races'] )
    for rano in range( nora ):
      sq_dic_3 = sq_dic_2.copy()
      this_race = reaj['RaceDay']['Meetings'][0]['Races'][rano]
########      print(this_race.keys() )
#= reaj['RaceDay']['Meetings'][0]['Races'][rano]

      sq_dic_3['Status'] = this_race['Status']
#
      if 'Tips' in  this_race:
#########        print( this_race['Tips'] )
        
#        sq_dic_3['Tips'] = this_race['Tips']
        tips = this_race['Tips'][0]
        sq_dic_3['Tips'] = tips['Tips']
#        sq_dic_3['Tips'] = this_race['Tips']

      sq_dic_3['RaceNumber'] = this_race['RaceNumber']
      sq_dic_3['RaceTime'] = this_race['RaceTime']
      sq_dic_3['RaceName'] = re.sub( "'", "^",this_race['RaceName'] )
      sq_dic_3['Distance'] = this_race['Distance']
      if 'WeatherCondition' in this_race:
        sq_dic_3['WeatherCondition'] = re.sub( "'", "^",this_race['WeatherCondition'] )
        sq_dic_3['TrackCondition'] = re.sub( "'", "^",this_race['TrackCondition'] )
        sq_dic_3['TrackRating'] = this_race['TrackRating']

      noru = len( this_race['Runners'] )
      for runo in range( noru ):
        sq_dic = sq_dic_3.copy()
        this_run = this_race['Runners'][runo]
        sq_dic['RunnerName'] = re.sub( "'", "^",this_run['RunnerName'] )
        sq_dic['RunnerNumber'] = this_run['RunnerNumber']
        sq_dic['RiderName'] = re.sub( "'", "^",this_run['RiderName'] )
        sq_dic['RiderChanged'] = this_run['RiderChanged']
        sq_dic['Barrier'] = this_run['Barrier']
        sq_dic['Weight'] = this_run['Weight']
        sq_dic['LastThreeStarts'] = re.sub( "'", "^",this_run['LastThreeStarts'] )
        sq_dic['Form'] = re.sub( "'", "^",this_run['Form'] )
        sq_dic['Rating'] = this_run['Rating']
        sq_dic['WinOdds'] = this_run['WinOdds']
        sq_dic['PlaceOdds'] = this_run['PlaceOdds']
        sq_dic['FPWinOdds'] = this_run['FPWinOdds']
        sq_dic['FPPlaceOdds'] = this_run['FPPlaceOdds']
        sq_dic['Scratched'] = this_run['Scratched']
        sq_dic['LateScratching'] = this_run['LateScratching']
#
#        debug
#
#        print(sq_dic)
#        print( sq_dic_3['Tips'] )
        lib_sql_write( con,curs,table,sq_dic )
#  print(len(reaj))
  con.commit()
  con.close()
exit()

